//
//  MappingTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class MappingTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRatesResponseMapping() {
        // given
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: "RatesResponse", ofType: "json") else {
            XCTFail()
            return
        }

        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail()
            return
        }

        let expectedBaseCurrency = Currency.default

        // when
        let decoder = JSONDecoder()
        let ratesResponse = try? decoder.decode(RatesResponse.self, from: data)
        // then
        XCTAssertNotNil(ratesResponse)
        XCTAssertEqual(ratesResponse?.baseCurrency, expectedBaseCurrency)
        XCTAssertEqual(ratesResponse?.rates.count, 32)
        XCTAssertEqual(ratesResponse?.rates.first?.baseCurrency, expectedBaseCurrency)
    }
}
