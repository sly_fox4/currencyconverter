//
//  ConverterTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class ConverterTests: XCTestCase {

    var converter: Converter!

    var delegate: MockConverterDelegate!

    override func setUp() {
        super.setUp()

        delegate = MockConverterDelegate()
        converter = Converter(service: TestRatesRequestor())
        converter.delegate = delegate
    }

    override func tearDown() {
        super.tearDown()
    }

    func testConvert() {
        // given

        let testCurrencies: [Currency] = [Currency.default, Currency(code: "RUB")]
        let expectedValues: [Currency: Double] = [Currency.default: 1,
                                                  Currency(code: "RUB"): 77.555]

        // when
        converter.startRatesUpdating()
        let convetredCurrencies = converter.exchange(currencies: testCurrencies)
        // then
        convetredCurrencies.forEach { convertedCurrency in
            XCTAssertEqual(convertedCurrency.value,
                           expectedValues[convertedCurrency.currency],
                           "\(convertedCurrency.currency.code) convert error")
        }
    }

    func testChangedAmountConvert() {
        // given

        let testCurrencies: [Currency] = [Currency.default, Currency(code: "RUB")]
        let expectedValues: [Currency: Double] = [Currency.default: 1,
                                                  Currency(code: "RUB"): 77.555]
        let newAmount: Double = 1.35

        // when
        converter.startRatesUpdating()
        converter.update(amount: newAmount)
        let convetredCurrencies = converter.exchange(currencies: testCurrencies)

        // then
        convetredCurrencies.forEach { convertedCurrency in
            XCTAssertEqual(convertedCurrency.value,
                           expectedValues[convertedCurrency.currency]! * newAmount,
                           "\(convertedCurrency.currency.code) convert error")
        }
    }

    func testZeroConvert() {
        // given

        let testCurrencies: [Currency] = [Currency.default, Currency(code: "RUB")]
        let expectedValue = 0.0

        let newAmount: Double = 0.0

        // when

        converter.startRatesUpdating()
        converter.update(amount: newAmount)
        let convetredCurrencies = converter.exchange(currencies: testCurrencies)

        // then
        convetredCurrencies.forEach { convertedCurrency in
            XCTAssertEqual(convertedCurrency.value,
                           expectedValue,
                           "\(convertedCurrency.currency.code) convert error")
        }
    }

    func testDelegateWasCalledOnCurrenciesUpdate() {
        // given

        let delegateExpecation = expectation(description: #function)

        delegate.didUpdateCurrenciesCallback = { [weak delegateExpecation] in
            delegateExpecation?.fulfill()
        }

        // when
        converter.startRatesUpdating()

        // then

        waitForExpectations(timeout: 0.1, handler: nil)
    }

    func testDelegateWasCalledOnRatesUpdate() {
        // given

        let delegateExpecation = expectation(description: #function)

        delegate.didUpdateRatesCallback = { [weak delegateExpecation] in
            delegateExpecation?.fulfill()
        }

        // when
        converter.startRatesUpdating()

        // then

        waitForExpectations(timeout: 0.1, handler: nil)
    }


    final class MockConverterDelegate: RatesConverterDelegate {

        var didUpdateCurrenciesCallback: EmptyClosure?
        var didUpdateRatesCallback: EmptyClosure?

        func converter(_: RatesConverter, didUpdate currencies: [Currency]) {
            didUpdateCurrenciesCallback?()
        }

        func converter(_: RatesConverter, didUpdate rates: [Rate]) {
            didUpdateRatesCallback?()
        }
    }
}
