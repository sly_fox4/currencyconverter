//
//  RatesServiceTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class RatesServiceTests: XCTestCase {

    var service: RatesService!

    override func setUp() {
        super.setUp()
        service = RatesService()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRateRequest() {
        // when
        let requestExpectation = expectation(description: #function)

        service.requestRates(for: Currency.default) { (result) in
            switch result {
            case .success(let response):
                XCTAssert(response.rates.count > 0)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            requestExpectation.fulfill()
        }

        // then
        waitForExpectations(timeout: 2.0, handler: nil)
    }
}
