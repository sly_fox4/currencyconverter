//
//  ConverterViewTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class ConverterViewTests: XCTestCase {

    private var view: ConverterViewController!
    private var output: ConverterViewOutputMock!

    override func setUp() {
        super.setUp()
        view = ConverterViewController()
        output = ConverterViewOutputMock()
        view.output = output
    }

    override func tearDown() {
        super.tearDown()
        view = nil
        output = nil
    }

    func testThatViewNotifiesPresenterOnDidLoad() {
        // when
        view.viewDidLoad()

        // then
        XCTAssert(output.viewLoadedWasCalled == true)
    }

    func testThatViewNotifiesPresenterOnCurrencySelect() {
        // when
        view.converterViewTableAdapter(ConverterViewTableAdapter(),
                                       didSelect: ConvertedCurrency(currency: Currency.default, value: 123))
        // then
        XCTAssert(output.selectCurrencyWasCalled == true)
    }

    func testThatViewNotifiesPresenterOnAmountChange() {
        // when
        let amount: String = "123"
        view.converterViewTableAdapter(ConverterViewTableAdapter(), didChange: amount)
        // then
        XCTAssert(output.changeAmountWasCalled == true)
    }

    final class ConverterViewOutputMock: ConverterViewOutput {

        var viewLoadedWasCalled: Bool = false
        var selectCurrencyWasCalled: Bool = false
        var changeAmountWasCalled: Bool = false

        func viewLoaded() {
            viewLoadedWasCalled = true
        }

        func select(currency: ConvertedCurrency) {
            selectCurrencyWasCalled = true
        }

        func change(amount: String) {
            changeAmountWasCalled = true
        }
    }

}
