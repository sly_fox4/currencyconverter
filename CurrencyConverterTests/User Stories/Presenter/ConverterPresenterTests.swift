//
//  ConverterPresenterTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class ConverterPresenterTests: XCTestCase {

    private var presenter: ConverterPresenter!
    private var view: MockViewController!
    private var router: MockRouter!

    private var converter: MockConverter!

    override func setUp() {
        super.setUp()
        converter = MockConverter()
        presenter = ConverterPresenter()
        presenter.set(converter: converter)
        presenter.router = router
        view = MockViewController()
        presenter.view = view
    }

    func testThatPresenterHandlesViewLoadedEvent() {
        // when
        presenter.viewLoaded()
        // then
        XCTAssertTrue(view.setupInitialStateWasCalled)
        XCTAssertTrue(converter.startRatesUpdatingCalled)
        XCTAssertTrue(view.updateConvertedCurrenciesWasCalled)
    }

    func testThatPresenterChangeAmount() {
        // when
        presenter.change(amount: "123")
        // then
        XCTAssertTrue(converter.updateAmountCalled)
    }

    func testThatPresenterUpdateBaseCurrency() {
        // when
        presenter.viewLoaded()
        presenter.select(currency: ConvertedCurrency(currency: Currency.default, value: 123))
        // then
        XCTAssertTrue(converter.updateCurrencyCalled)
        XCTAssertTrue(view.focusOnCurrencyWasCalled)
    }

    final class MockRouter: ConverterRouterInput {
    }

    final class MockViewController: ConverterViewInput {
        var setupInitialStateWasCalled: Bool = false
        var focusOnCurrencyWasCalled: Bool = false
        var updateConvertedCurrenciesWasCalled: Bool = false

        func update(convertedCurrencies: [ConvertedCurrency]) {
            updateConvertedCurrenciesWasCalled = true
        }

        func focusOnCurrency(with index: Int) {
            focusOnCurrencyWasCalled = true
        }

        func setupInitialState() {
            setupInitialStateWasCalled = true
        }
    }

    final class MockConverter: RatesConverter {

        var startRatesUpdatingCalled: Bool = false
        var updateAmountCalled: Bool = false
        var updateCurrencyCalled: Bool = false

        var delegate: RatesConverterDelegate?

        func startRatesUpdating() {
            startRatesUpdatingCalled = true
            let rubleCurrency = Currency(code: "RUB")
            let currencies = [Currency.default, rubleCurrency]
            delegate?.converter(self, didUpdate: currencies)
            delegate?.converter(self, didUpdate: [Rate(baseCurrency: Currency.default, сurrency: rubleCurrency, rate: 75.49)])
        }

        func stopRatesUpdating() { }

        func update(baseCurrency: Currency, with amount: Double) {
            updateCurrencyCalled = true
        }

        func update(amount: Double) {
            updateAmountCalled = true
        }

        func exchange(currencies: [Currency]) -> [ConvertedCurrency] {
            return []
        }
    }

}
