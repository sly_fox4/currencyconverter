//
//  ConverterModuleConfiguratorTests.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class ConverterModuleConfiguratorTests: XCTestCase {

    func testConfigureScreen() {
        // when
        let viewController = ConverterModuleConfigurator().configure()

        // then
        XCTAssertNotNil(viewController.output, "ConverterViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ConverterPresenter, "output is not ConverterPresenter")

        let presenter: ConverterPresenter = viewController.output as! ConverterPresenter
        XCTAssertNotNil(presenter.view, "view in ConverterPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ConverterPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ConverterRouter, "router is not ConverterRouter")

        let router: ConverterRouter = presenter.router as! ConverterRouter
        XCTAssertTrue(router.view is ConverterViewController, "view in router is not ConverterViewController")
    }

}
