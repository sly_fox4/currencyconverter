//
//  TestRatesRequestor.swift
//  CurrencyConverterTests
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import XCTest
@testable import CurrencyConverter

final class TestRatesRequestor: RatesRequestor {
    func requestRates(for base: Currency, completion: @escaping GetRatesCompletion) {
        XCTAssertEqual(base, Currency.default, "Base currency must be Currency.default")

        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: "RatesResponse", ofType: "json") else {
            XCTFail()
            return
        }

        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail()
            return
        }
        let decoder = JSONDecoder()
        let ratesResponse = try! decoder.decode(RatesResponse.self, from: data)

        completion(.success(ratesResponse))
    }
}

