//
//  Rate.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 10.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

struct Rate {
    let baseCurrency: Currency
    let сurrency: Currency
    let rate: Double
}
