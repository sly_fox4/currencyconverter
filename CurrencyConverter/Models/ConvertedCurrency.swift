//
//  ConvertedCurrency.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

struct ConvertedCurrency {
    let currency: Currency
    let value: Double
    let textRepresentation: String?

    private static let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.roundingMode = .floor
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        formatter.usesGroupingSeparator = false

        return formatter
    }()

    init(currency: Currency, value: Double) {
        self.currency = currency
        self.value = value

        if value.truncatingRemainder(dividingBy: 1) == 0 {
            self.textRepresentation = String(format: "%.0f", value)
        } else {
            self.textRepresentation = ConvertedCurrency.formatter.string(from: NSNumber(value: value))
        }
    }
}
