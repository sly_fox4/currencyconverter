//
//  RatesResponse.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

struct RatesResponse {
    let baseCurrency: Currency
    let rates: [Rate]
}

// MARK: - Decodable

extension RatesResponse: Decodable {

    private enum CodingKeys: String, CodingKey {
        case baseCurrency = "base"
        case rates
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let baseCurrencyString = try container.decode(String.self, forKey: .baseCurrency)
        let base = Currency(code: baseCurrencyString)
        let ratesDictionary = try container.decode(Dictionary<String, Double>.self, forKey: .rates)
        let rates: [Rate] = ratesDictionary.map({ (key, value) -> Rate in
            let currency = Currency(code: key)
            return Rate(baseCurrency: base, сurrency: currency, rate: value)
        })

        self.init(baseCurrency: base, rates: rates)
    }
}
