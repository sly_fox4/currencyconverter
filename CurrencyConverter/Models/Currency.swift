//
//  Currency.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

struct Currency {

    let code: String

    var name: String? {
        return Locale.current.localizedString(forCurrencyCode: code)
    }

    static var `default`: Currency {
        return Currency(code: "EUR")
    }
}

// MARK: - Hashable

extension Currency: Hashable {
    var hashValue: Int {
        return code.hashValue
    }
}

// MARK: - Equatable

extension Currency: Equatable {
    static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code == rhs.code
    }
}
