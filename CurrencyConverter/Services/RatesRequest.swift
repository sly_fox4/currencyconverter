//
//  RatesRequest.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

final class RatesRequest: BaseServerRequest<RatesResponse> {

    private let baseCurrency: Currency

    init(baseCurrency: Currency) {
        self.baseCurrency = baseCurrency
    }

    override func createURLRequest() -> URLRequest {
        guard let url = URL(string: Urls.Rates.latest(baseCurrency.code)) else {
            fatalError()
        }

        return URLRequest(url: url)
    }

    override func handle(response: BaseServerResponse, with completion: @escaping (ResponseResult<RatesResponse>) -> Void) {
        let decoder = JSONDecoder()
        if let ratesResponse = try? decoder.decode(RatesResponse.self, from: response.data) {
            completion(.success(ratesResponse))
        } else {
            completion(.failure(BaseServerError.cantMapping))
        }
    }
}
