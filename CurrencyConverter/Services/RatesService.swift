//
//  RatesService.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

final class RatesService: RatesRequestor {

    func requestRates(for base: Currency, completion: @escaping GetRatesCompletion) {
        let request = RatesRequest(baseCurrency: base)
        request.perform(with: completion)
    }
}
