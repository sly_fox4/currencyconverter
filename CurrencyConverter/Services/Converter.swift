//
//  Exchanger.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

protocol RatesConverter {

    var delegate: RatesConverterDelegate? { get set }

    func startRatesUpdating()
    func stopRatesUpdating()
    func update(baseCurrency: Currency, with amount: Double)
    func update(amount: Double)
    func exchange(currencies: [Currency]) -> [ConvertedCurrency]
}

protocol RatesConverterDelegate: class {
    func converter(_: RatesConverter, didUpdate currencies: [Currency])
    func converter(_: RatesConverter, didUpdate rates: [Rate])
}

final class Converter: RatesConverter {

    // MARK: - Properties

    weak var delegate: RatesConverterDelegate?

    private var baseCurrency: Currency = Currency.default
    private var rates: [Rate] = []
    private var amount: Double = 1.0

    private var updateTask: DelayableTask?

    // MARK: - Constants

    private let service: RatesRequestor
    private let refreshRate: Double

    // MARK: - Initialization and deinitialization

    init(service: RatesRequestor, refreshRate: Double = 1.0) {
        self.service = service
        self.refreshRate = refreshRate
    }

    // MARK: - Internal methods

    func startRatesUpdating() {
        requestNewRates()
    }

    func stopRatesUpdating() {
        updateTask?.cancel()
        updateTask = nil
    }

    func update(baseCurrency: Currency, with amount: Double) {
        self.baseCurrency = baseCurrency
        update(amount: amount)
    }

    func update(amount: Double) {
        self.amount = amount
    }

    func exchange(currencies: [Currency]) -> [ConvertedCurrency] {
        var convertedCurrencies: [ConvertedCurrency] = [ConvertedCurrency(currency: baseCurrency, value: amount)]
        for currency in currencies {
            if let rate = rates.first(where: { $0.сurrency == currency }) {
                let converted = ConvertedCurrency(currency: currency, value: amount * rate.rate)
                convertedCurrencies.append(converted)
            }
        }

        return convertedCurrencies
    }

    // MARK: - Private methods

    private func requestNewRates() {
        service.requestRates(for: baseCurrency) { [weak self] result in
            guard let `self` = self else {
                return
            }

            switch result {
            case .failure:
                // Place to handle errors
                break
            case .success(let ratesResponse):
                self.rates = ratesResponse.rates

                var newCurrencies = [self.baseCurrency]
                newCurrencies.append(contentsOf: ratesResponse.rates.map({ $0.сurrency }))

                DispatchQueue.main.async {
                    self.delegate?.converter(self, didUpdate: newCurrencies)
                    self.delegate?.converter(self, didUpdate: ratesResponse.rates)
                    self.updateTask = DelayableTask(seconds: self.refreshRate, action: { [weak self] in
                        self?.requestNewRates()
                    })
                }
            }
        }
    }
}
