//
//  Urls.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

enum Urls {
    static let base = "https://revolut.duckdns.org"

    enum Rates {
        static func latest(_ p1: String) -> String {
            return String(format: "\(base)/latest?base=%@", p1)
        }
    }
}
