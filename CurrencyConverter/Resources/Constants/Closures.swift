//
//  Closures.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 11.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

typealias EmptyClosure = () -> Void
typealias StringClosure = (String) -> Void
