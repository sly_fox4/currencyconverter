//
//  ConverterRouter.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

final class ConverterRouter: ConverterRouterInput {

    // MARK: - Properties

    weak var view: ModuleTransitionable?
}
