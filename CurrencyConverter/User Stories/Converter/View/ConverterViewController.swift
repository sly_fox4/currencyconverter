//
//  ConverterViewController.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 09.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

final class ConverterViewController: UIViewController, ModuleTransitionable, ConverterViewInput {

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties

    var output: ConverterViewOutput?

    private var adapter: ConverterViewTableAdapter?

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewLoaded()
    }

    // MARK: - ConverterViewInput

    func setupInitialState() {
        configure()
    }

    func update(convertedCurrencies: [ConvertedCurrency]) {
        adapter?.update(convertedCurrencies: convertedCurrencies)
    }

    func focusOnCurrency(with index: Int) {
        adapter?.focusOnRow(with: index)
    }

}

// MARK: - ConverterViewTableAdapterDelegate

extension ConverterViewController: ConverterViewTableAdapterDelegate {
    func converterViewTableAdapter(_ :ConverterViewTableAdapter, didSelect currency: ConvertedCurrency) {
        output?.select(currency: currency)
    }

    func converterViewTableAdapter(_ :ConverterViewTableAdapter, didChange amount: String) {
        output?.change(amount: amount)
    }
}

// MARK: - Configuration

private extension ConverterViewController {

    /// Configure view
    func configure() {
        configureAdapter()
        configureAppearance()
    }

    func configureAdapter() {
        adapter = ConverterViewTableAdapter()
        adapter?.set(tableView: tableView)
        adapter?.delegate = self
    }

    /// Configures Appearance of the view
    func configureAppearance() {
        title = "Currency Converter"
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorColor = .clear
    }
}
