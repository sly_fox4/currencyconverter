//
//  ConverterViewInput.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 10.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

protocol ConverterViewInput: class {
    func setupInitialState()
    func update(convertedCurrencies: [ConvertedCurrency])
    func focusOnCurrency(with index: Int)
}
