//
//  CurrencyCell.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 09.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

final class CurrencyCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!

    // MARK: - Properties

    var didChangeAmount: StringClosure?

    // MARK: - Initialization and deinitialization

    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }

    // MARK: - UITableViewCell

    override func becomeFirstResponder() -> Bool {
        textField.isUserInteractionEnabled = true
        return textField.becomeFirstResponder()
    }

    // MARK: - Internal methods

    func configure(with convertedCurrency: ConvertedCurrency) {
        titleLabel.text = convertedCurrency.currency.code
        subtitleLabel.text = convertedCurrency.currency.name
        textField.text = convertedCurrency.textRepresentation
    }

}

// MARK: - UITextFieldDelegate

extension CurrencyCell: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.isUserInteractionEnabled = false
    }

    @objc
    private func textFieldTextDidChange(sender: UITextField) {
        didChangeAmount?(sender.text ?? "")
    }
}

// MARK: - Configuration

private extension CurrencyCell {

    /// Configure view
    func configure() {
        textField.isUserInteractionEnabled = false
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldTextDidChange(sender:)), for: .editingChanged)
        configureAppearance()
    }

    /// Configures Appearance of the view
    func configureAppearance() {
        selectionStyle = .none
        textField.keyboardType = .decimalPad
        textField.borderStyle = .none
        textField.placeholder = "0"
        textField.textAlignment = .right
        textField.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
        titleLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        subtitleLabel.font = UIFont.systemFont(ofSize: 11.0, weight: .regular)
        subtitleLabel.textColor = UIColor.lightGray
    }
}
