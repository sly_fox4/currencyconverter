//
//  ConverterViewOutput.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 10.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

protocol ConverterViewOutput: class {
    func viewLoaded()
    func select(currency: ConvertedCurrency)
    func change(amount: String)
}
