//
//  ConverterViewTableAdapter.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 10.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

protocol ConverterViewTableAdapterDelegate: class {
    func converterViewTableAdapter(_ :ConverterViewTableAdapter, didSelect currency: ConvertedCurrency)
    func converterViewTableAdapter(_ :ConverterViewTableAdapter, didChange amount: String)
}

final class ConverterViewTableAdapter: NSObject {

    // MARK: - Nested types

    private enum Constants {
        static let estimatedRowHeight: CGFloat = 60.0
        static let scrollToRowAnimationTime = 0.25
    }

    // MARK: - Properties

    weak var delegate: ConverterViewTableAdapterDelegate?

    private weak var tableView: UITableView?

    private var currencies: [ConvertedCurrency] = []

    private var indexPathsToUpdate: [IndexPath] {
        var indexPaths = [IndexPath]()
        for index in 1..<currencies.count {
            indexPaths.append(IndexPath(row: index, section: 0))
        }
        return indexPaths
    }

    // MARK: - Internal methods

    func set(tableView: UITableView?) {
        self.tableView = tableView
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.estimatedRowHeight = Constants.estimatedRowHeight
        tableView?.register(CurrencyCell.self)
    }

    func update(convertedCurrencies: [ConvertedCurrency]) {
        let shouldReloadAll = currencies.isEmpty
        currencies = convertedCurrencies
        if shouldReloadAll {
            tableView?.reloadData()
        } else {
            UIView.performWithoutAnimation {
                self.tableView?.reloadRows(at: indexPathsToUpdate, with: .none)
            }
        }
    }

    func focusOnRow(with index: Int) {
        let indexPath = self.indexPath(with: index)
        let firstRowIndex = 0
        let firstRowIndexPath = self.indexPath(with: firstRowIndex)

        let focused = currencies.remove(at: index)
        currencies.insert(focused, at: 0)

        tableView?.beginUpdates()
        tableView?.moveRow(at: indexPath, to: firstRowIndexPath)
        tableView?.endUpdates()
        UIView.animate(withDuration: Constants.scrollToRowAnimationTime, animations: {
            self.tableView?.scrollToRow(at: firstRowIndexPath, at: .top, animated: false)
        }) { _ in
            self.tableView?.cellForRow(at: firstRowIndexPath)?.becomeFirstResponder()
        }
    }

    // MARK: - Private methods

    /// Index path for row index in table view
    ///
    /// - Parameter index: index of row
    /// - Returns: IndexPath for row
    func indexPath(with index: Int) -> IndexPath {
        return IndexPath(row: index, section: 0)
    }
}

// MARK: - UITableViewDelegate

extension ConverterViewTableAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.converterViewTableAdapter(self, didSelect: currencies[indexPath.row])
    }
}

// MARK: - UITableViewDataSource

extension ConverterViewTableAdapter: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: CurrencyCell = tableView.dequeueReusableCell(for: indexPath) else {
            return UITableViewCell()
        }

        cell.configure(with: currencies[indexPath.row])

        cell.didChangeAmount = { [weak self] amount in
            guard let `self` = self else {
                return
            }
            self.delegate?.converterViewTableAdapter(self, didChange: amount)
        }

        return cell
    }
}
