//
//  ConverterPresenter.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 10.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

final class ConverterPresenter: ConverterViewOutput, ConverterModuleInput {

    // MARK: - Properties

    weak var view: ConverterViewInput?
    var router: ConverterRouterInput?

    private var converter: RatesConverter?

    /// Current currencies list
    private var currencies: [Currency] = []

    // MARK: - ConverterViewOutput

    func viewLoaded() {
        converter?.delegate = self
        view?.setupInitialState()
        converter?.startRatesUpdating()
    }

    func select(currency: ConvertedCurrency) {
        guard let index = currencies.index(of: currency.currency) else {
            return
        }

        let selected = currencies.remove(at: index)
        currencies.insert(selected, at: 0)
        converter?.update(baseCurrency: selected, with: currency.value)
        view?.focusOnCurrency(with: index)
    }

    func change(amount: String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal

        if let amountDoubleValue = formatter.number(from: amount)?.doubleValue {
            converter?.update(amount: amountDoubleValue)
        } else {
            converter?.update(amount: 0)
        }
        converte()
    }

    // MARK: - ConverterModuleInput

    func set(converter: RatesConverter) {
        self.converter = converter
    }

    // MARK: - Private methods

    private func converte() {
        if let convertedCurrencies = converter?.exchange(currencies: self.currencies) {
            view?.update(convertedCurrencies: convertedCurrencies)
        }
    }
}

// MARK: - ConverterDelegate

extension ConverterPresenter: RatesConverterDelegate {
    func converter(_: RatesConverter, didUpdate currencies: [Currency]) {
        let removedCurrencies = Set(self.currencies).subtracting(Set(currencies))
        let newCurrencies = Set(currencies).subtracting(Set(self.currencies))
        self.currencies = self.currencies.filter({ !removedCurrencies.contains($0) })
        self.currencies.append(contentsOf: newCurrencies)
    }

    func converter(_: RatesConverter, didUpdate rates: [Rate]) {
        converte()
    }
}
