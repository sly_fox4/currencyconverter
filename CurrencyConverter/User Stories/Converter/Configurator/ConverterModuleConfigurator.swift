//
//  ConverterModuleConfigurator.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 09.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

final class ConverterModuleConfigurator {

    // MARK: - Internal methods

    func configure() -> ConverterViewController {
        guard let view = UIStoryboard(name: String(describing: ConverterViewController.self),
                                      bundle: Bundle.main).instantiateInitialViewController() as? ConverterViewController else {
            fatalError("Can't load ConverterViewController from storyboard, check that controller is initial view controller")
        }

        let presenter = ConverterPresenter()
        let router = ConverterRouter()

        presenter.set(converter: Converter(service: RatesService()))

        view.output = presenter
        presenter.view = view
        presenter.router = router
        router.view = view

        return view
    }

}
