//
//  RatesRequestor.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

typealias GetRatesCompletion = (ResponseResult<RatesResponse>) -> Void

protocol RatesRequestor {
    func requestRates(for base: Currency, completion: @escaping GetRatesCompletion)
}
