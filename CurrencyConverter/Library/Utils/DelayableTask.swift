//
//  DelayableTask.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

class DelayableTask {

    private let action: () -> Void
    private var isCancelled = false
    private let queue: DispatchQueue

    init(seconds: Double, queue: DispatchQueue = DispatchQueue.main, action: @escaping () -> Void) {
        self.action = action
        self.queue = queue
        exec(after: seconds)
    }

    deinit {
        cancel()
    }

    private func exec(after seconds: Double) {
        let time = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        queue.asyncAfter(deadline: time) {
            if !self.isCancelled {
                self.action()
            }
        }
    }

    func cancel() {
        isCancelled = true
    }
}
