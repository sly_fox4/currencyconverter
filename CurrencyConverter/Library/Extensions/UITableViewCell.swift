//
//  UITableViewCell.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class var identifier: String {
        return String(describing: self)
    }
}
