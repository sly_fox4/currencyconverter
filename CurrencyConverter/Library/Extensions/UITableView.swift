//
//  UITableView.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 13.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

extension UITableView {

    func register(_ cellType: UITableViewCell.Type) {
        self.register(UINib(nibName: cellType.identifier, bundle: nil), forCellReuseIdentifier: cellType.identifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T
    }
}
