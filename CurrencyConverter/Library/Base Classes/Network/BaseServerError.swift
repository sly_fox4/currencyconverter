//
//  BaseServerError.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

enum BaseServerError: Error {
    case undefind
    case cantMapping
}
