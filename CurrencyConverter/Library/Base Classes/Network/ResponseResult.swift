//
//  ResponseResult.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

enum ResponseResult<Value> {
    case success(Value)
    case failure(Error)
}
