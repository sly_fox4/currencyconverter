//
//  BaseServerRequest.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 12.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import Foundation

open class BaseServerRequest<ResultValueType> {

    typealias RequestCompletion = (ResponseResult<ResultValueType>) -> Void

    private var urlRequest: URLRequest {
        return createURLRequest()
    }

    func perform(with completion: @escaping RequestCompletion) {
        _ = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in

            if let error = error {
                completion(.failure(error))
                return
            }

            guard let data = data, let response = response else {
                completion(.failure(BaseServerError.undefind))
                return
            }

            self.handle(response: BaseServerResponse(data: data, response: response), with: completion)
        }.resume()
    }

    func createURLRequest() -> URLRequest {
        preconditionFailure("This method must be overriden by the subclass")
    }

    func handle(response: BaseServerResponse, with completion: @escaping RequestCompletion) {
        preconditionFailure("This method must be overriden by the subclass")
    }
}
