//
//  AppDelegate.swift
//  CurrencyConverter
//
//  Created by Serge Nanaev on 09.08.2018.
//  Copyright © 2018 Serge Nanaev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        initializeRootView()

        return true
    }

    private func initializeRootView() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let initialViewController = ConverterModuleConfigurator().configure()
        window?.rootViewController = UINavigationController(rootViewController: initialViewController)
        window?.makeKeyAndVisible()
    }

}
